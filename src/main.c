#include <stdio.h>
#include <vector.h>

int main()
{
	float x,y,z;
	
	float v_dotProduct,v_magnitud1,v_magnitud2;
	vector3D v_crossProduct;
	int ortogonal;
	
	
	
	printf("Ingrese las coordenadas del punto #1\n");
	printf("x: ");
	scanf("%f", &x);
	printf("y: ");	
	scanf("%f", &y);
	printf("z: ");
	scanf("%f", &z);
	vector3D v1={.x=x,.y=y,.z=z};
	
	printf("\nIngrese las coordenadas del punto #2\n");
	printf("x: ");
	scanf("%f",&x);
	printf("y: ");	
	scanf("%f", &y);
	printf("z: ");
	scanf("%f", &z);
	vector3D v2={.x=x,.y=y,.z=z};
	
	v_dotProduct= dotProduct(v1,v2);
	v_crossProduct = crossProduct(v1,v2);
	v_magnitud1 = magnitud(v1);
	v_magnitud2 = magnitud(v2);
	ortogonal = esOrtogonal(v1,v2);
	
	printf("\nEl producto punto entre los vectores es: %.3f",v_dotProduct);
	printf("\nEl producto cruz entre los dos vectores es:  ( %3.f , %3.f ,%3.f ) ",v_crossProduct.x,v_crossProduct.y , v_crossProduct.z);
	printf("\nLa magnitud del vector 1 es: %.3f", v_magnitud1);
	printf("\nLa magnitud del vector 2 es: %.3f", v_magnitud2);
	if (ortogonal ==1) 
		printf("\nLos vectores 1 y 2 son ortogonales\n");
	else
		printf("\nLos vectores no son ortogonales\n");
}
