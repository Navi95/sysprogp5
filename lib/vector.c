/* implementar aquí las funciones requeridas */
#include <stdio.h>
#include "vector.h"
#include <math.h>

float dotProduct(vector3D v1, vector3D v2)
{
	return (v1.x*v2.x)+(v1.y*v2.y)+(v1.z*v2.z);		
}

vector3D crossProduct(vector3D v1, vector3D v2)
{
	float i=(v1.y*v2.z)-(v1.z*v2.y);
	float j=(v1.x*v2.z)-(v1.z*v2.x);
	float k=(v1.x*v2.y)-(v1.y*v2.x);
	vector3D result={.x=i, .y=j, .z=k};

	return result;
}

float magnitud(vector3D vector)
{
	
	float sumaCuadrados = pow(vector.x,2) + pow(vector.y,2) + pow(vector.z,2);
	float v_magnitud = sqrt(sumaCuadrados);
	return v_magnitud; 
}

int esOrtogonal(vector3D vector1, vector3D vector2)
{
	
	if (dotProduct(vector1,vector2 ) == 0)
	{
		return 1;
	}
	return 0;
	
}
