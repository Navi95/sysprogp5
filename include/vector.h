/* Definicion del tipo de datos vector3D */
#include <stdlib.h>

typedef struct {
	float x,y,z;
		
} vector3D;

float dotProduct(vector3D vector1, vector3D vector2);
vector3D crossProduct(vector3D vector1, vector3D vector2);
float magnitud(vector3D vector);
int esOrtogonal(vector3D vector1, vector3D vector2);
