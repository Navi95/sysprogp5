CC=gcc

IDIR =./include
VECDIR = ./lib/vector.c
MAINDIR = ./src/main.c

CFLAGS=-I$(IDIR)

LIBS=-lm
OBJ = main.o vector.o


%.o:
	$(CC) -c $(VECDIR) $(CFLAGS)
	$(CC) -c -Wall $(MAINDIR) $(CFLAGS)	
	

all: $(OBJ)
	$(CC) -o vector $^  $(LIBS) $(CFLAGS) 

static:
	ar rcs libvector.a vector.o
	$(CC) -static -o $@ main.o libvector.a $(LIBS)

dynamic: 
	$(CC) -shared -fPIC -o libvector.so $(VECDIR) $(LIBS) $(CFLAGS)
	$(CC) -o $@ $(MAINDIR) ./libvector.so $(LIBS) $(CFLAGS)


.PHONY: clean

clean:
	rm -f *.o vector static dynamic
